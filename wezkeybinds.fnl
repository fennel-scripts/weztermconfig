(local wezterm (require :wezterm))
(local act wezterm.action)

{:key_tables
 {:copy_mode
  [{:action
	(act.CopyMode :MoveForwardWord)
	:key :Tab
	:mods :NONE}
   {:action (act.CopyMode :MoveBackwardWord)
	:key :Tab
	:mods :SHIFT}
   {:action (act.CopyMode :MoveToStartOfNextLine)
	:key :Enter
	:mods :NONE}
   {:action (act.CopyMode :Close)
	:key :Escape
	:mods :NONE}
   {:action (act.CopyMode {:SetSelectionMode :Cell})
	:key :Space
	:mods :NONE}
   {:action (act.CopyMode :MoveToEndOfLineContent)
	:key "$"
	:mods :NONE}
   {:action (act.CopyMode :MoveToEndOfLineContent)
	:key "$"
	:mods :SHIFT}
   {:action (act.CopyMode :JumpReverse)
	:key ","
	:mods :NONE}
   {:action (act.CopyMode :MoveToStartOfLine)
	:key :0
	:mods :NONE}
   {:action (act.CopyMode :JumpAgain)
	:key ";"
	:mods :NONE}
   {:action (act.CopyMode {:JumpBackward {:prev_char false}})
	:key :F
	:mods :NONE}
   {:action (act.CopyMode {:JumpBackward {:prev_char false}})
	:key :F
	:mods :SHIFT}
   {:action (act.CopyMode :MoveToScrollbackBottom)
	:key :G
	:mods :NONE}
   {:action (act.CopyMode :MoveToScrollbackBottom)
	:key :G
	:mods :SHIFT}
   {:action (act.CopyMode :MoveToViewportTop)
	:key :H
	:mods :NONE}
   {:action (act.CopyMode :MoveToViewportTop)
	:key :H
	:mods :SHIFT}
   {:action (act.CopyMode :MoveToViewportBottom)
	:key :L
	:mods :NONE}
   {:action (act.CopyMode :MoveToViewportBottom)
	:key :L
	:mods :SHIFT}
   {:action (act.CopyMode :MoveToViewportMiddle)
	:key :M
	:mods :NONE}
   {:action (act.CopyMode :MoveToViewportMiddle)
	:key :M
	:mods :SHIFT}
   {:action (act.CopyMode :MoveToSelectionOtherEndHoriz)
	:key :O
	:mods :NONE}
   {:action (act.CopyMode :MoveToSelectionOtherEndHoriz)
	:key :O
	:mods :SHIFT}
   {:action (act.CopyMode {:JumpBackward {:prev_char true}})
	:key :T
	:mods :NONE}
   {:action (act.CopyMode {:JumpBackward {:prev_char true}})
	:key :T
	:mods :SHIFT}
   {:action (act.CopyMode {:SetSelectionMode :Line})
	:key :V
	:mods :NONE}
   {:action (act.CopyMode {:SetSelectionMode :Line})
	:key :V
	:mods :SHIFT}
   {:action (act.CopyMode :MoveToStartOfLineContent)
	:key "^"
	:mods :NONE}
   {:action (act.CopyMode :MoveToStartOfLineContent)
	:key "^"
	:mods :SHIFT}
   {:action (act.CopyMode :MoveBackwardWord)
	:key :b
	:mods :NONE}
   {:action (act.CopyMode :MoveBackwardWord)
	:key :b
	:mods :ALT}
   {:action (act.CopyMode :PageUp) :key :b :mods :CTRL}
   {:action (act.CopyMode :Close) :key :c :mods :CTRL}
   {:action (act.CopyMode {:MoveByPage 0.5})
	:key :d
	:mods :CTRL}
   {:action (act.CopyMode :MoveForwardWordEnd)
	:key :e
	:mods :NONE}
   {:action (act.CopyMode {:JumpForward {:prev_char false}})
	:key :f
	:mods :NONE}
   {:action (act.CopyMode :MoveForwardWord)
	:key :f
	:mods :ALT}
   {:action (act.CopyMode :PageDown)
	:key :f
	:mods :CTRL}
   {:action (act.CopyMode :MoveToScrollbackTop)
	:key :g
	:mods :NONE}
   {:action (act.CopyMode :Close) :key :g :mods :CTRL}
   {:action (act.CopyMode :MoveLeft)
	:key :h
	:mods :NONE}
   {:action (act.CopyMode :MoveDown)
	:key :j
	:mods :NONE}
   {:action (act.CopyMode :MoveUp) :key :k :mods :NONE}
   {:action (act.CopyMode :MoveRight)
	:key :l
	:mods :NONE}
   {:action (act.CopyMode :MoveToStartOfLineContent)
	:key :m
	:mods :ALT}
   {:action (act.CopyMode :MoveToSelectionOtherEnd)
	:key :o
	:mods :NONE}
   {:action (act.CopyMode :Close) :key :q :mods :NONE}
   {:action (act.CopyMode {:JumpForward {:prev_char true}})
	:key :t
	:mods :NONE}
   {:action (act.CopyMode {:MoveByPage (- 0.5)})
	:key :u
	:mods :CTRL}
   {:action (act.CopyMode {:SetSelectionMode :Cell})
	:key :v
	:mods :NONE}
   {:action (act.CopyMode {:SetSelectionMode :Block})
	:key :v
	:mods :CTRL}
   {:action (act.CopyMode :MoveForwardWord)
	:key :w
	:mods :NONE}
   {:action (act.Multiple [{:CopyTo :ClipboardAndPrimarySelection}
						   {:CopyMode :Close}])
	:key :y
	:mods :NONE}
   {:action (act.CopyMode :PageUp)
	:key :PageUp
	:mods :NONE}
   {:action (act.CopyMode :PageDown)
	:key :PageDown
	:mods :NONE}
   {:action (act.CopyMode :MoveToEndOfLineContent)
	:key :End
	:mods :NONE}
   {:action (act.CopyMode :MoveToStartOfLine)
	:key :Home
	:mods :NONE}
   {:action (act.CopyMode :MoveLeft)
	:key :LeftArrow
	:mods :NONE}
   {:action (act.CopyMode :MoveBackwardWord)
	:key :LeftArrow
	:mods :ALT}
   {:action (act.CopyMode :MoveRight)
	:key :RightArrow
	:mods :NONE}
   {:action (act.CopyMode :MoveForwardWord)
	:key :RightArrow
	:mods :ALT}
   {:action (act.CopyMode :MoveUp)
	:key :UpArrow
	:mods :NONE}
   {:action (act.CopyMode :MoveDown)
	:key :DownArrow
	:mods :NONE}]
  :search_mode
  [
   {:action	(act.CopyMode :PriorMatch)
	:key :Enter
	:mods :NONE}
   {:action (act.CopyMode :Close)
	:key :Escape
	:mods :NONE}
   {:action (act.CopyMode :NextMatch)
	:key :n
	:mods :CTRL}
   {:action (act.CopyMode :PriorMatch)
	:key :p
	:mods :CTRL}
   {:action (act.CopyMode :CycleMatchType)
	:key :r
	:mods :CTRL}
   {:action (act.CopyMode :ClearPattern)
	:key :u
	:mods :CTRL}
   {:action (act.CopyMode :PriorMatchPage)
	:key :PageUp
	:mods :NONE}
   {:action (act.CopyMode :NextMatchPage)
	:key :PageDown
	:mods :NONE}
   {:action (act.CopyMode :PriorMatch)
	:key :UpArrow
	:mods :NONE}
   {:action (act.CopyMode :NextMatch)
	:key :DownArrow
	:mods :NONE}]}
 :keys
 [
  {:action (act.ActivateTabRelative 1) :key :Tab :mods :CTRL}
  {:action (act.ActivateTabRelative (- 1)) :key :Tab :mods "SHIFT|CTRL"}
  {:action act.ToggleFullScreen :key :Enter :mods :ALT}
  {:action (act.ActivateTab 0) :key "!" :mods :CTRL}
  {:action (act.ActivateTab 0) :key "!" :mods "SHIFT|CTRL"}
  {:action (act.SplitVertical {:domain :CurrentPaneDomain})
   :key "\""
   :mods "ALT|CTRL"}
  {:action (act.SplitVertical {:domain :CurrentPaneDomain})
   :key "\""
   :mods "SHIFT|ALT|CTRL"}
  {:action (act.ActivateTab 2) :key "#" :mods :CTRL}
  {:action (act.ActivateTab 2) :key "#" :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 3) :key "$" :mods :CTRL}
  {:action (act.ActivateTab 3) :key "$" :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 4) :key "%" :mods :CTRL}
  {:action (act.ActivateTab 4) :key "%" :mods "SHIFT|CTRL"}
  {:action (act.SplitHorizontal {:domain :CurrentPaneDomain})
   :key "%"
   :mods "ALT|CTRL"}
  {:action (act.SplitHorizontal {:domain :CurrentPaneDomain})
   :key "%"
   :mods "SHIFT|ALT|CTRL"}
  {:action (act.ActivateTab 6) :key "&" :mods :CTRL}
  {:action (act.ActivateTab 6) :key "&" :mods "SHIFT|CTRL"}
  {:action (act.SplitVertical {:domain :CurrentPaneDomain})
   :key "'"
   :mods "SHIFT|ALT|CTRL"}
  {:action (act.ActivateTab (- 1)) :key "(" :mods :CTRL}
  {:action (act.ActivateTab (- 1)) :key "(" :mods "SHIFT|CTRL"}
  {:action act.ResetFontSize :key ")" :mods :CTRL}
  {:action act.ResetFontSize :key ")" :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 7) :key "*" :mods :CTRL}
  {:action (act.ActivateTab 7) :key "*" :mods "SHIFT|CTRL"}
  {:action act.IncreaseFontSize :key "+" :mods :CTRL}
  {:action act.IncreaseFontSize :key "+" :mods "SHIFT|CTRL"}
  {:action act.DecreaseFontSize :key "-" :mods :CTRL}
  {:action act.DecreaseFontSize :key "-" :mods "SHIFT|CTRL"}
  {:action act.ResetFontSize :key :0 :mods :CTRL}
  {:action act.ResetFontSize :key :0 :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 0) :key :1 :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 1) :key :2 :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 2) :key :3 :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 3) :key :4 :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 4) :key :5 :mods "SHIFT|CTRL"}
  {:action (act.SplitHorizontal {:domain :CurrentPaneDomain}) :key :5	 :mods "SHIFT|ALT|CTRL"}
  {:action (act.ActivateTab 5) :key :6 :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 6) :key :7 :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 7) :key :8 :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab (- 1)) :key :9 :mods "SHIFT|CTRL"}
  {:action act.IncreaseFontSize :key "=" :mods :CTRL}
  {:action act.IncreaseFontSize :key "=" :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 1) :key "@" :mods :CTRL}
  {:action (act.ActivateTab 1) :key "@" :mods "SHIFT|CTRL"}
  {:action (act.CopyTo :Clipboard) :key :C :mods :CTRL}
  {:action (act.CopyTo :Clipboard) :key :C :mods "SHIFT|CTRL"}
  {:action (act.Search :CurrentSelectionOrEmptyString)
   :key :F
   :mods :CTRL}
  {:action (act.Search :CurrentSelectionOrEmptyString)
   :key :F
   :mods "SHIFT|CTRL"}
  {:action (act.ClearScrollback :ScrollbackOnly) :key :K :mods :CTRL}
  {:action (act.ClearScrollback :ScrollbackOnly)
   :key :K
   :mods "SHIFT|CTRL"}
  {:action act.ShowDebugOverlay :key :L :mods :CTRL}
  {:action act.ShowDebugOverlay :key :L :mods "SHIFT|CTRL"}
  {:action act.Hide :key :M :mods :CTRL}
  {:action act.Hide :key :M :mods "SHIFT|CTRL"}
  {:action act.SpawnWindow :key :N :mods :CTRL}
  {:action act.SpawnWindow :key :N :mods "SHIFT|CTRL"}
  {:action act.ActivateCommandPalette :key :P :mods :CTRL}
  {:action act.ActivateCommandPalette :key :P :mods "SHIFT|CTRL"}
  {:action act.ReloadConfiguration :key :R :mods :CTRL}
  {:action act.ReloadConfiguration :key :R :mods "SHIFT|CTRL"}
  {:action (act.SpawnTab :CurrentPaneDomain) :key :T :mods :CTRL}
  {:action (act.SpawnTab :CurrentPaneDomain) :key :T :mods "SHIFT|CTRL"}
  {:action (act.CharSelect {:copy_on_select true
							:copy_to :ClipboardAndPrimarySelection})
   :key :U
   :mods :CTRL}
  {:action (act.CharSelect {:copy_on_select true
							:copy_to :ClipboardAndPrimarySelection})
   :key :U
   :mods "SHIFT|CTRL"}
  {:action (act.PasteFrom :Clipboard) :key :V :mods :CTRL}
  {:action (act.PasteFrom :Clipboard) :key :V :mods "SHIFT|CTRL"}
  {:action (act.CloseCurrentTab {:confirm true}) :key :W :mods :CTRL}
  {:action (act.CloseCurrentTab {:confirm true})
   :key :W
   :mods "SHIFT|CTRL"}
  {:action act.ActivateCopyMode :key :X :mods :CTRL}
  {:action act.ActivateCopyMode :key :X :mods "SHIFT|CTRL"}
  {:action act.TogglePaneZoomState :key :Z :mods :CTRL}
  {:action act.TogglePaneZoomState :key :Z :mods "SHIFT|CTRL"}
  {:action (act.ActivateTab 5) :key "^" :mods :CTRL}
  {:action (act.ActivateTab 5) :key "^" :mods "SHIFT|CTRL"}
  {:action act.DecreaseFontSize :key "_" :mods :CTRL}
  {:action act.DecreaseFontSize :key "_" :mods "SHIFT|CTRL"}
  {:action (act.CopyTo :Clipboard) :key :c :mods "SHIFT|CTRL"}
  {:action (act.Search :CurrentSelectionOrEmptyString)
   :key :f
   :mods "SHIFT|CTRL"}
  {:action (act.ClearScrollback :ScrollbackOnly)
   :key :k
   :mods "SHIFT|CTRL"}
  {:action act.ShowDebugOverlay :key :l :mods "SHIFT|CTRL"}
  {:action act.Hide :key :m :mods "SHIFT|CTRL"}
  {:action act.SpawnWindow :key :n :mods "SHIFT|CTRL"}
  {:action act.ActivateCommandPalette :key :p :mods "SHIFT|CTRL"}
  {:action act.ReloadConfiguration :key :r :mods "SHIFT|CTRL"}
  {:action (act.SpawnTab :CurrentPaneDomain) :key :t :mods "SHIFT|CTRL"}
  {:action (act.CharSelect {:copy_on_select true
							:copy_to :ClipboardAndPrimarySelection})
   :key :u
   :mods "SHIFT|CTRL"}
  {:action (act.PasteFrom :Clipboard) :key :v :mods "SHIFT|CTRL"}
  {:action (act.CloseCurrentTab {:confirm true})
   :key :w
   :mods "SHIFT|CTRL"}
  {:action act.ActivateCopyMode :key :x :mods "SHIFT|CTRL"}
  {:action act.TogglePaneZoomState :key :z :mods "SHIFT|CTRL"}
  {:action act.QuickSelect :key "phys:Space" :mods "SHIFT|CTRL"}
  {:action (act.ScrollByPage (- 1)) :key :PageUp :mods :SHIFT}
  {:action (act.ActivateTabRelative (- 1)) :key :PageUp :mods :CTRL}
  {:action (act.ActivateTabRelative 1) :key :PageUp :mods "SHIFT|CTRL"}
  {:action (act.ScrollByPage 1) :key :PageDown :mods :SHIFT}
  {:action (act.ActivateTabRelative 1) :key :PageDown :mods :CTRL}
  {:action (act.ActivateTabRelative (- 1))
   :key :PageDown
   :mods "SHIFT|CTRL"}
  {:action (act.ActivateTabRelative (- 1))
   :key :LeftArrow
   :mods "SHIFT|CTRL"}
  {:action (act.AdjustPaneSize [:Left 1])
   :key :LeftArrow
   :mods "SHIFT|ALT|CTRL"}
  {:action (act.ActivateTabRelative 1)
   :key :RightArrow
   :mods "SHIFT|CTRL"}
  {:action (act.AdjustPaneSize [:Right 1])
   :key :RightArrow
   :mods "SHIFT|ALT|CTRL"}
  {:action (act.ActivatePaneDirection :Up)
   :key :UpArrow
   :mods "SHIFT|CTRL"}
  {:action (act.AdjustPaneSize [:Up 1])
   :key :UpArrow
   :mods "SHIFT|ALT|CTRL"}
  {:action (act.ActivatePaneDirection :Down)
   :key :DownArrow
   :mods "SHIFT|CTRL"}
  {:action (act.AdjustPaneSize [:Down 1])
   :key :DownArrow
   :mods "SHIFT|ALT|CTRL"}
  {:action (act.PasteFrom :PrimarySelection) :key :Insert :mods :SHIFT}
  {:action (act.CopyTo :PrimarySelection) :key :Insert :mods :CTRL}
  {:action (act.CopyTo :Clipboard) :key :Copy :mods :NONE}
  {:action (act.PasteFrom :Clipboard) :key :Paste :mods :NONE}
  {:action (act.ActivateTabRelative (- 1)) :key :BrowserBack :mods :NONE}
  {:action (act.ActivateTabRelative 1) :key :BrowserForward :mods :NONE}]}
