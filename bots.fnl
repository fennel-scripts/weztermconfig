#!/usr/bin/env fennel
(local bots {})

(fn exec [tbl]
  (os.execute (table.concat tbl " ")))


(local target "~/.config/wezterm/")

(local files ["theme" "wezterm" "wezkeybinds"])

(fn bots.build [_]
  (each [i file (ipairs files)]
	  (exec [:fennel
			 "--compile "
			 ;;"--metadata" "--correlate"
			 (.. file :.fnl)
			 ">"
			 (.. file :.lua)])))

(fn bots.dbg [_]
  (each [i file (ipairs files)]
	  (exec [:fennel
			 "--compile "
			 ;;"--metadata"
			 "--correlate"
			 (.. file :.fnl)
			 ">"
			 (.. file :.lua)])))



(fn bots.install [_]
  (each [i file (ipairs files)]
	(do
	  (exec ["cp" "-rf" "-v" (.. file :.lua) (.. target file :.lua)]))))

(fn bots.run [_]
  (exec ["clear"])
  (exec ["wezterm" "start" "--class=tmp" "sleep 1"])
  )

(fn bots.clean [_]
  (exec [:rm :-f :*.lua]))

(fn bots.default []
  (bots:clean)
  (bots:build)
  (bots:install)
  (bots:clean))

(fn bots.debug []
  (bots:dbg)
  (bots:install)
  ;;(bots:clean)
  )



(bots.build)
(bots.install)
(bots.clean)

;;bots
