#!/usr/bin/env fennel

;;; code:

;;wezterm.nerdfonts.

;;; code to grab the correct battery icons acording to state
(fn getBatIcon [battery]
	"TODO write something here"
	(local batIcons
		 {
		  :discharging
		  {
		   :10 wezterm.nerdfonts.mdi_battery_10
		   :20 wezterm.nerdfonts.mdi_battery_20
		   :30 wezterm.nerdfonts.mdi_battery_30
		   :40 wezterm.nerdfonts.mdi_battery_40
		   :50 wezterm.nerdfonts.mdi_battery_50
		   :60 wezterm.nerdfonts.mdi_battery_60
		   :70 wezterm.nerdfonts.mdi_battery_70
		   :80 wezterm.nerdfonts.mdi_battery_80
		   :90 wezterm.nerdfonts.mdi_battery_90
		   :alert wezterm.nerdfonts.mdi_battery_alert
		   :full  wezterm.nerdfonts.mdi_battery
		   }
		  :charging
		  {
		   :10    wezterm.nerdfonts.mdi_battery_charging_10
		   :20    wezterm.nerdfonts.mdi_battery_charging_20
		   :30    wezterm.nerdfonts.mdi_battery_charging_30
		   :40    wezterm.nerdfonts.mdi_battery_charging_40
		   :50    wezterm.nerdfonts.mdi_battery_charging_50
		   :60    wezterm.nerdfonts.mdi_battery_charging_60
		   :70    wezterm.nerdfonts.mdi_battery_charging_70
		   :80    wezterm.nerdfonts.mdi_battery_charging_80
		   :90    wezterm.nerdfonts.mdi_battery_charging_90
		   :full  wezterm.nerdfonts.mdi_battery_charging_100
		   :alert wezterm.nerdfonts.mdi_battery_alert
		   }
		  }
		 )
	(local bat battery)

	(fn isCharging []
	(if (. bat :chargingQ) :charging :discharging))
;;;;; this could have been a swich-case statement, but it isnt
	(local message (if (= (. bat :message) nil) "bat: "))
	(local charge (. bat :charge))
	(if
	 (< 9  charge )	(.. message (. (. batIcons (isCharging)) :alert))
	 (< 10 charge )	(.. message (. (. batIcons (isCharging)) :10))
	 (< 20 charge )	(.. message (. (. batIcons (isCharging)) :20))
	 (< 30 charge )	(.. message (. (. batIcons (isCharging)) :30))
	 (< 40 charge )	(.. message (. (. batIcons (isCharging)) :40))
	 (< 50 charge )	(.. message (. (. batIcons (isCharging)) :50))
	 (< 60 charge )	(.. message (. (. batIcons (isCharging)) :60))
	 (< 70 charge )	(.. message (. (. batIcons (isCharging)) :70))
	 (< 80 charge )	(.. message (. (. batIcons (isCharging)) :80))
	 (< 90 charge )	(.. message (. (. batIcons (isCharging)) :90)))
	(<= 91 charge )	(.. message (. (. batIcons (isCharging)) :full))
	)
;;!		Use < and <= but avoid > and >=.
;;!		Interpret < as "are the numbers in increasing order?"
;;!		not "is the second number greater than the first number?"
;;!		Rationale: it's easy to get mixed up between prefix-form < and >
;;!		if you think of them as "greater than" and "less than",
;;!		especially since in infix languages people are used to making the big
;;!		end point to the larger value, which doesn't work in prefix notation.
;;!		But in Fennel, the < operator can take any number of arguments,
;;!		so it's really asking whether the arguments are in increasing order.
;;!		The > operator asks whether the arguments are sorted in reverse order,
;;!		which is less intuitive.




;; (for [i 1 100 5]
;;   (local fo-battery {:chargingQ true :charge i})
;;     (print i	 (getBatIcon fo-battery))
;; )

;; (for [i 1 100 5]
;;   (local fo-battery {:chargingQ false :charge i})
;;     (print i	 (getBatIcon fo-battery))
;; )
;;  (local nerd wezterm.nerdfonts)
;;; set the style of the statusbar

(fn stateToIsCharging [state]
	(if (= state "Charging") true)
	(if (= state "Discharging") false)
	(if (= state "Empty" ) true)
	(if (= state "Full") false)
	;; Unknown"
	)


(wezterm.on
 :update-right-status
 (fn [window pane]
	 (let [cells {}]
	 (var cwd-uri (pane:get_current_working_dir))
	 (when cwd-uri
		 (set cwd-uri (cwd-uri:sub 8))
		 (local slash (cwd-uri:find "/"))
		 (var cwd "")
		 (var hostname "")
		 (when slash
		 (set hostname (cwd-uri:sub 1 (- slash 1)))
		 (local dot (hostname:find "[.]"))
		 (when dot
			 (set hostname (hostname:sub 1 (- dot 1))))
		 (set cwd (cwd-uri:sub slash))
		 (table.insert cells cwd)
		 ;;(table.insert cells hostname)
		 ))
	 (local date
			(.. wezterm.nerdfonts.mdi_clock "  "
				(wezterm.strftime "%a %b %-d %H:%M")))
	 (table.insert cells date)
	 ;;TODO change to function that gives a nicer indicator
	 (each [_ b (ipairs (wezterm.battery_info))]
		 (local fo-battery
				{:isCharging (stateToIsCharging b.state)
				 :charge (* b.state_of_charge 100)})
		 (local batstatus
						(.. (getBatIcon fo-battery)
								(string.format " %.0f%%" (* b.state_of_charge 100))))
		 (table.insert cells batstatus))

	 (local LEFT_ARROW (utf8.char 57523))
	 (local SOLID_LEFT_ARROW (utf8.char 57522))
	 (local colors ["#3c1361"	"#52307c" "#663a82" "#7c5295" "#b491c8"])
	 (local text-fg "#c0c0c0")
	 (local elements {})
	 (var num-cells 0)
	 (local push
			(fn [text is-last]
				(let [cell-no (+ num-cells 1)]
				(table.insert elements
								{:Foreground {:Color text-fg}})
				(table.insert elements
								{:Background {:Color (. colors cell-no)}})
				(table.insert elements {:Text (.. " " text " ")})
				(when (not is-last)
					(table.insert elements {:Foreground {:Color (. colors (+ cell-no 1))}})
					(table.insert elements {:Text SOLID_LEFT_ARROW})
					)
				(set num-cells (+ num-cells 1)))))
	 (while (> (length cells) 0)
		 (local cell (table.remove cells 1))
		 (push cell (= (length cells) 0)))
	 (window:set_right_status (wezterm.format elements)))))
