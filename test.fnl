#!/usr/bin/env fennel
;;; code to grab the correct battery icons acording to state
;;(fn getBatIcon [battery]
  ;;"TODO write something here"
  (local batIcons
		 {
		  :discharging
		  {
		   :10			:wezterm.nerdfonts.mdi_battery_10
		   :20			:wezterm.nerdfonts.mdi_battery_20
		   :30			:wezterm.nerdfonts.mdi_battery_30
		   :40			:wezterm.nerdfonts.mdi_battery_40
		   :50			:wezterm.nerdfonts.mdi_battery_50
		   :60			:wezterm.nerdfonts.mdi_battery_60
		   :70			:wezterm.nerdfonts.mdi_battery_70
		   :80			:wezterm.nerdfonts.mdi_battery_80
		   :90			:wezterm.nerdfonts.mdi_battery_90
		   :alert		:wezterm.nerdfonts.mdi_battery_alert
		   :full		:wezterm.nerdfonts.mdi_battery
		   }
		  :charging
		  {
		   :10			:wezterm.nerdfonts.mdi_battery_charging_10
		   :20			:wezterm.nerdfonts.mdi_battery_charging_20
		   :30			:wezterm.nerdfonts.mdi_battery_charging_30
		   :40			:wezterm.nerdfonts.mdi_battery_charging_40
		   :50			:wezterm.nerdfonts.mdi_battery_charging_50
		   :60			:wezterm.nerdfonts.mdi_battery_charging_60
		   :70			:wezterm.nerdfonts.mdi_battery_charging_70
		   :80			:wezterm.nerdfonts.mdi_battery_charging_80
		   :90			:wezterm.nerdfonts.mdi_battery_charging_90
		   :full		:wezterm.nerdfonts.mdi_battery_charging_100
		   :alert		:wezterm.nerdfonts.mdi_battery_alert
		   }})
(local battery
	   {
		:message "battery:"
		:charge 28
		}
	   )


(local bat battery)

(fn isCharging []
  (if (. bat :charging?) :charging :discharging)
  )
;;;;; this could have been a swich-case statement, but it isnt
(local message (if (= (. bat :message) nil) "bat: "))
(local charge (. bat :charge))

  (if (> charge 9)
	  (if (> charge 10)
		  (if (> charge 20)
			  (if (> charge 30)
				  (if (> charge 40)
					  (if (> charge 50)
						  (if (> charge 60)
							  (if (> charge 70)
								  (if (> charge 80)
									  (if (> charge 90)
										  (if (>= charge 91)
											  (.. message (. (. batIcons (isCharging)) :full)))
										  (.. message (. (. batIcons (isCharging)) :90)))
									  (.. message (. (. batIcons (isCharging)) :80)))
								  (.. message (. (. batIcons (isCharging)) :70)))
							  (.. message (. (. batIcons (isCharging)) :60)))
						  (.. message (. (. batIcons (isCharging)) :50)))
					  (.. message (. (. batIcons (isCharging)) :40)))
				  (.. message (. (. batIcons (isCharging)) :30)))
			  (.. message (. (. batIcons (isCharging)) :20)))
		  (.. message (. (. batIcons (isCharging)) :10)))
	  (.. message (. (. batIcons (isCharging)) :alert)));; end of first if





;;; code to grab the correct battery icons acording to state
(fn getBatIcon [battery]
  "TODO write something here"
  (local batIcons
		 {
		  :discharging
		  {
		   :10		:mdi_battery_10
		   :20		:mdi_battery_20
		   :30		:mdi_battery_30
		   :40		:mdi_battery_40
		   :50		:mdi_battery_50
		   :60		:mdi_battery_60
		   :70		:mdi_battery_70
		   :80		:mdi_battery_80
		   :90		:mdi_battery_90
		   :alert	:mdi_battery_alert
		   :full	:mdi_battery
		   }
		  :charging
		  {
		   :10		:mdi_battery_charging_10
		   :20		:mdi_battery_charging_20
		   :30		:mdi_battery_charging_30
		   :40		:mdi_battery_charging_40
		   :50		:mdi_battery_charging_50
		   :60		:mdi_battery_charging_60
		   :70		:mdi_battery_charging_70
		   :80		:mdi_battery_charging_80
		   :90		:mdi_battery_charging_90
		   :full	:mdi_battery_charging_100
		   :alert	:mdi_battery_alert
		   }}))
