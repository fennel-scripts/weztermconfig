#!/usr/bin/env fennel
;; -*- indent-tabs-mode: allways; tab-width: 2; -*-


(local wezterm (require :wezterm))
(local theme (require :theme ))

(local colours  theme.colours)
(local myTheme  theme.myTheme)
(local mycolors theme.mycolors)


(local act wezterm.action)
(local wezsuper "CTRL|SHIFT")

;;; description
;;this is the configuration file for wezterm terminal emulator
;;


;;(local theme :PaleNighthC)

																				;   "Andromeda")
(local progs
	   {
		:sshmenu	{:label	"ssh-menu"			:args ["sshmenu"]}
		:mount		{:label "mount"				:args ["bashmount"]}
		})




;;; launch menu variable setup
(local launchMenu
	   [{:label "htop"	:args ["htop"]
		 ;;:cwd ;;working directory
		 }
		{:label "xplr"	:args ["xplr"]
		 ;;:cwd ;;working directory
		 }
		progs.sshmenu
		progs.mount
		])

;;; fonts
(local fontUsed
		 (wezterm.font_with_fallback
		[
		 "UbuntuMono Nerd Font"
		 "Ubuntu Mono Ligaturized"
		 ;;"FiraCode Nerd Font"
		 "JetBrains Mono"
		 ;;{:weight "Bold" :italic true}
		 ]
		)
		 )
(local visual-bell
		 {
		:fade_in_function "EaseIn"
		:fade_in_duration_ms 150
		:fade_out_function "EaseOut"
		:fade_out_duration_ms 150
		:target "CursorColor"
		})

(fn ssh-domain [args]
	{
	 :name (or args.name args.hostname)
	 :remote_address (or args.hostname args.ip nil)
	 :username (or args.username args.user "erik")
	 }
	)
(fn ip [last]
	(.. "192.168.0." last))

(local ssh-domains
			 [(ssh-domain {:name "home-server" :hostname "server.local" :ip (ip :101)})])

(local unix-domains
	   [{:name "default"}
			{:name "scratch"}
			{:name "music"}
			{:name "explorer"}
			{:name "compilation"}
			{:name "status"}])



;;; :
(local ncpamixer
		 {:label "volumectrl"
			:args ["ncpamixer"]})

(local mopidy
		 {:label "music daemon"
			:args ["mopidy"]})

(local musicplayer
		 {:label "musicplayer"
			:args ["ncmpcpp"]})

(local cava
		 {:label "audio visualiser"
			:args ["cava"]})

(local musicAction
       {:Multiple
       [(wezterm.action {:SpawnCommandInNewTab mopidy})
	;;(wezterm.action {:ActivateTab 1})
	(wezterm.action {:SpawnCommandInNewTab musicplayer})
	(wezterm.action {:SpawnCommandInNewTab cava})]})




;;; keybinds
(fn keybindings []
	[
	 ;;{:key "m" :mods "SUPER" :action "DisableDefaultAssignment"}
	 {:mods wezsuper	:key "PageUp"			:action  act.IncreaseFontSize}
	 {:mods wezsuper	:key "PageDown"			:action  act.DecreaseFontSize}
	 {:mods wezsuper	:key "LeftArrow"		:action (act.ActivateTabRelative (- 1))}
	 {:mods wezsuper	:key "RightArrow"		:action (act.ActivateTabRelative 1)}
	 {:mods wezsuper	:key ">"				:action (act.ShowLauncherArgs {:flags "FUZZY|WORKSPACES"})}
	 {:mods "CTRL"		:key "n"				:action (act.SwitchWorkspaceRelative 1)}
	 {:mods "CTRL"		:key "p"				:action (act.SwitchWorkspaceRelative (- 1))}
	 ;;{:key "X"						:mods "CTRL"		:action wezterm.action.ActivateCopyMode }
	 ;;{:key "X"						:mods "CTRL"		:action wezterm.action.ActivateCopyMode }

	 {:key "BrowserBack"		:action (act.ActivateTabRelative (- 1))}
	 {:key "BrowserForward"		:action (act.ActivateTabRelative 1)}
	 ]
	)
;;; (top)statusbar
(local bar {})
(fn bar.right [window pane]
  (local powerline
		 {:arrow
		  {:left {:solid (utf8.char 0xe0b2) :normal (utf8.char 0xe0b3)}
		   ;;		   :right
		   }})
	
  (local cells [])
  (local info {})
  (local time
		 {:date (wezterm.strftime " %Y-%m-%d ")
		  :time (wezterm.strftime " %H:%M ")})
	
  (fn addcell [text {: bg : fg}]
	(local index 1)
	(table.insert cells {:Foreground {:Color (or fg mycolors.colour3)}} )
	(table.insert cells {:Background {:Color (or bg mycolors.colour2)}} )
	(table.insert cells {:Text text} )
	;;	(table.insert cells )
	)
  (local c mycolors)
  (local t myTheme)
  (local text-fg c.colour4)
  (local palette
		 [{:text {:fg text-fg	:bg t.red}
		   :arrow {:fg t.red	:bg t.bg}}
		  {:text {:fg t.red		:bg t.green}
		   :arrow {:fg t.green	:bg t.red}}
		  {:text {:fg t.red		:bg t.yellow}
		   :arrow {:fg t.yellow	:bg t.green}}
		  {:text {:fg text-fg	:bg t.red}
		   :arrow {:fg t.red	:bg t.text}}
		  {:text {:fg text-fg	:bg t.green}
		   :arrow {:fg t.red	:bg t.green}}])



  (local steps
		 [(.. " [" (or (window:active_workspace) "!") "]")
		  (.. " [" (or (window:window_id) "!") "] ")
		  time.time
		  time.date])

  (each [k value (ipairs steps)]
	(local v (. palette k))
	;;(addcell powerline.arrow.left.solid v.arrow)
	  (addcell powerline.arrow.left.solid v.arrow)
		(addcell value v.text))
  (window:set_right_status (wezterm.format cells)))

;;(fn bar.left [window pane])


(wezterm.on "update-right-status" bar.right)



;;; general settings
(local opacity 0.8)
(local scrollbar false)
;;(var textBgOpacity 0.9)

(local exit
	 {:close "Close"
	  :hold "Hold"
	  :closeOnClean "CloseOnCleanExit"})



(local keybinds (require :wezkeybinds))

(local keys keybinds.keys)
(each [key binding (ipairs (keybindings))]
  (table.insert keys binding))


;;(local key-tables keybinds.key_tables) (each [key binding (ipairs (keybindings))] (table.insert key-tables binding))

;;; this is equivalent to a return statement
{
 :disable_default_key_bindings true
 :keys keys
 ;;(keybindings)
 ;;:key_tables keybinds.key_tables
 :launch_menu launchMenu
 ;; :color_scheme theme		;dont use
 :visual_bell visual-bell
 :audible_bell "Disabled"
 :font fontUsed
 :font_size 16
 :adjust_window_size_when_changing_font_size false ;fix bug when zooming/changing font-size on awesome
 :ssh_domains ssh-domains
 :unix_domains unix-domains
 ;;:default_prog ["/usr/bin/zsh"]
 :default_prog ["/usr/local/bin/hilbish"]
 :colors colours
 :window_background_opacity opacity
 ;; :text_background_opacity textBgOpacity
 :enable_scroll_bar scrollbar
 :use_fancy_tab_bar false
 :exit_behavior exit.close
 }
